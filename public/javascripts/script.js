var demriApp = angular.module('demriApp', ['ngRoute']);

// configure our routes
demriApp.config(function ($routeProvider) {
    $routeProvider

    // route for the home page
        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'mainController'
        })

        // route for the about page
        .when('/about', {
            templateUrl: 'pages/about.html',
            controller: 'aboutController'
        })

        .when('/rpc', {
            templateUrl: 'pages/connectivity.html',
            controller: 'rpcController'
        })
        .when('/contract/:contractName', {
            templateUrl: 'pages/contract.html',
            controller: 'contractController'
        })

        .when('/explorer', {
            templateUrl: 'pages/explorer.html',
            controller: 'explorerController'
        })

        // route for the contact page
        .when('/team', {
            templateUrl: 'pages/team.html',
            controller: 'teamController'
        })
        .when('/addContract', {
            templateUrl: 'pages/newcontract.html',
            controller: 'newContractCtrl'
        });
});

demriApp.controller('mainController', function ($scope, $http, $interval) {

    $scope.callAtTimeout = function() {
        $http.get("/rpc/blockNumber")
            .then(function (response) {
                $scope.blockNumber = response.data.block;
            });
    };

   $interval( function(){ $scope.callAtTimeout(); }, 10000);
    $scope.callAtTimeout();
});

demriApp.controller('aboutController', function ($scope) {
    $scope.message = 'Look! I am an about page.';
});

demriApp.controller('teamController', function ($scope) {
    $scope.message = 'Igor oren itai or ohad';
});

demriApp.controller('rpcController', function ($scope, $http) {
    $scope.loading = true;
    $http.get("/rpc/data")
        .then(function (response) {
            $scope.data = response.data;
            $scope.data.title = $scope.data.connected ? 'Connected' : 'Not Connected';
            $scope.data.css = $scope.data.connected ? 'panel panel-success' : 'panel panel-danger';
            $scope.loading = false;
        });
});

demriApp.controller('newContractCtrl', function ($scope, $http) {

    $scope.error = false;
    $scope.compiled = false;
    $scope.compileClick = function () {
        $http.post("/rpc/contract/compile", {data: $scope.contractData})
            .then(function (response) {
                $scope.error = !response.data.success;
                $scope.errorMessage = response.data.message;
                $scope.name = response.data.name;
                $scope.compiled = response.data.success;
            });
    };

    $scope.deployClick = function () {
        $http.post("/rpc/contract/deploy", {data: $scope.contractData})
            .then(function (response) {
               console.log(response.data);
                $scope.deployError = response.data.deployError
                $scope.name = response.data.name;
                $scope.contractDeployed = response.data.contractDeployed;

            });
    };
});

demriApp.controller('explorerController', function ($scope, $http) {

    $scope.refreshContracts =function (){
        $http.get("/rpc/contract/data/all", {data: $scope.contractData})
            .then(function (response) {
                $scope.records = response.data;
            });

    }
    $scope.refreshContracts();
});

demriApp.controller('contractController', function ($scope, $http) {

});

