var demriApp = angular.module('demriApp', ['ngRoute']);

// configure our routes
demriApp.config(function ($routeProvider) {

$routeProvider.when('/', {
        templateUrl: 'pages/home.html',
        controller: 'mainController'
    })
    .when('/isdx/companies', {
        templateUrl: 'pages/explorer.html',
        controller: 'explorerController'
    })

    .when('/isdx/company/:compId', {
        templateUrl: 'pages/company.html',
        controller: 'compController'
    })


    .when('/isdx/trades', {
        templateUrl: 'pages/trades.html',
        controller: 'tradesController'
    })
    .when('/isdx/trades/:compId', {
        templateUrl: 'pages/trades.html',
        controller: 'tradesController'
    })
    .when('/isdx/trades/:compId/:tradeId', {
        templateUrl: 'pages/trades.html',
        controller: 'tradesController'
    })

    .when('/isdx/holders', {
        templateUrl: 'pages/holders.html',
        controller: 'holdersController'
    })
    .when('/isdx/holder/:holderId', {
        templateUrl: 'pages/holder.html',
        controller: 'holderController'
    });
});


demriApp.controller('mainController', function ($scope ,$http, $interval) {


    $scope.callAtTimeout = function() {
        $http.get("/rpc/contract/isdx/isdxTransactions")
            .then(function (response) {
                $scope.tx = response.data.tx;
            });
    };

    $interval( function(){ $scope.callAtTimeout(); }, 10000);
    $scope.callAtTimeout();
});

demriApp.controller('aboutController', function ($scope) {
    $scope.message = 'Look! I am an about page.';
});

demriApp.controller('tradesController', function ($scope,$http,$routeParams,$timeout) {

    $http.get("/rpc/contract/events/ISDX/shareHolder")
        .then(function (response) {
            $scope.holders = response.data;
        });
    $http.get("/rpc/contract/events/ISDX/companies")
        .then(function (response) {
            $scope.companies = response.data;
        });
    $scope.showModal = false;
    $scope.formData = {};
    $scope.toggleModal = function(){
        $scope.showModal = !$scope.showModal;
    };
    
    $scope.submitTrade = function () {
        $scope.sendingData = true;
        $scope.formData.contract = 'ISDX';
        $scope.formData.function = 'addTrade';
        
        
        console.log('name ',$scope.formData);
        $http.post("/rpc/contract/ISDX/addTrade", $scope.formData)
            .then(function (response) {
                console.log('hash:',response.data);
                $scope.showModal = false;
                $scope.sendingData = false;
                $scope.formData = {};
                $timeout(function() {console.log('timeout ');$scope.refreshTrades()},7000);
            }, function () {
                $scope.sendingData = false;
            });
        
    }

    $scope.relevantId = $routeParams.tradeId ? $routeParams.tradeId : 0;
    $scope.refreshTrades = function (){
    
        var url = '/rpc/contract/events/ISDX/trades' + (($routeParams.compId) ? '/' + $routeParams.compId : '');
        console.log(url);
        $http.get(url)
            .then(function (response) {
                $scope.records = response.data;
            });

    };
    $scope.refreshTrades();
    $scope.txModal = false;
     {};
    $scope.openTxModal =function (txData) {
        $scope.tx =txData;    
        $scope.txModal = true;
    };
    $scope.closeTx =function () {
        $scope.txModal = false;    
    };

});

demriApp.controller('holdersController', function ($scope, $http,$timeout) {
    $scope.showModal = false;
    $scope.formData = {};
    $scope.toggleModal = function(){
        $scope.showModal = !$scope.showModal;
    };
    $scope.submitHolder = function (){
        $scope.sendingData = true;
        console.log('name ',$scope.formData);
        $http.post("/rpc/contract/execute/ISDX/addShareHolder", $scope.formData)
            .then(function (response) {
                console.log('hash:',response.data);
                $scope.showModal = false;
                $scope.sendingData = false;
                $scope.formData = {};
                $timeout(function() {console.log('timeout ');$scope.refreshHolders()},7000);
            }, function () {
                $scope.sendingData = false;
            });

    };
   
    $scope.refreshHolders =function (){
        $http.get("/rpc/contract/events/ISDX/shareHolder")
            .then(function (response) {
                var resa=[];
                angular.forEach(response.data, function(val, key) {
                    resa.push({id:key,name:val});
                });
                $scope.records  = resa.sort(function(a,b){return   a.id - b.id;});
            });

    }
    $scope.refreshHolders();
});


demriApp.controller('explorerController', function ($scope, $http,$timeout) {

    $scope.showModal = false;
    $scope.formData = {};
    $scope.toggleModal = function(){
        $scope.showModal = !$scope.showModal;
    };

    $http.get("/rpc/contract/events/ISDX/shareHolder")
        .then(function (response) {
            $scope.holders = response.data;
        });
    $scope.submitComp = function(){
        $scope.sendingData = true;
        console.log('name ',$scope.formData);
        $http.post("/rpc/contract/execute/ISDX/registerCompany", $scope.formData)
            .then(function (response) {
                console.log('hash:',response.data);
                $scope.showModal = false;
                $scope.sendingData = false;
                $scope.formData = {};
                $timeout(function() {console.log('timeout ');$scope.refreshComp()},7000);
            }, function () {
                $scope.sendingData = false;
            });
       

    };
    $scope.refreshComp =function (){
        $http.get("/rpc/contract/ISDX/companies/data")
            .then(function (response) {
                $scope.records = response.data;
            });

    }
    $scope.refreshComp();
});

demriApp.directive('modal', function () {
    return {
        template: '<div class="modal fade">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
        '<h4 class="modal-title">{{ title }}</h4>' +
        '</div>' +
        '<div class="modal-body" ng-transclude></div>' +
        '</div>' +
        '</div>' +
        '</div>',
        restrict: 'E',
        transclude: true,
        replace:true,
        scope:true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;

            scope.$watch(attrs.visible, function(value){
                if(value == true)
                    $(element).modal('show');
                else
                    $(element).modal('hide');
            });

            $(element).on('shown.bs.modal', function(){
                scope.$apply(function(){
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function(){
                scope.$apply(function(){
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});

demriApp.controller('compController', function ($scope, $http,$routeParams) {
    $http.get('/rpc/contract/ISDX/company/'+$routeParams.compId)
        .then(function (response) {
            $scope.data = response.data;
            $scope.data.totalShares = parseInt(response.data.totalShares);
            $scope.trades = $scope.data.trades.sort(function(a,b){return b.id - a.id;});
        });
    $http.get('/rpc/contract/events/ISDX/balance/'+$routeParams.compId)
        .then(function (response) {

            var resa=[];
              angular.forEach(response.data, function(val, key) {
                resa.push({name:key,balance:val});
            });
            $scope.balances = resa.sort(function(a,b){return b.balance - a.balance;});
        });

    $scope.registerEvent= function(type){
        console.log(type);
    }
});

demriApp.controller('holderController', function ($scope, $http,$routeParams) {
    $http.get('/rpc/contract/ISDX/getHolderInfo/'+$routeParams.holderId)
        .then(function (response) {
            $scope.data = response.data;
        });



    $scope.registerEvent= function(type){
        console.log(type);
    }
});


