var express = require('express');
var router = express.Router();
var fs = require('fs');

var rpc = require('./rpc');

var Web3 = require('web3');


var rpcNode = 'http://ec2-54-173-23-102.compute-1.amazonaws.com:8541';

var web3 = new Web3(new Web3.providers.HttpProvider(rpcNode));


var compiledContracts = {};
var contracts = {};
var deployed = {};
var events = {};
var cache = {};

// only for isdx PoC . TODO: remove.
module.exports.eventData = events;
//

contracts["Trades"] = 'contract Trades {mapping (uint => string) public trades; uint public counter ; function Trades() {counter = 0; } function addTrade(string doc){trades[counter] = doc; counter++; } function get(uint id) constant returns(string){return trades[id]; } }';


router.get('/deploy/:name', function (req, res, next) {

    var compiledContract = compiledContracts[req.params.name];
    var coin;
    web3.eth.contract(compiledContract.abi).new({
        from: web3.eth.accounts[0],
        data: compiledContract.code,
        gas: 2000000
    }, function (err, contInstance) {
        if (err) {
            console.error(err);
        } else if (contInstance.address) {
            deployed[req.params.name] = contInstance;
            console.log('address : ' + contInstance.address);
            console.log('deployed : ', req.params.name);
        }
    });

    res.send('contract ' + req.params.name + ' deployed and not mined yet .<br><a href="/rpc/contract/' + req.params.name + '/functions" >click to check the contract status</a>');

});


router.get('/:name', function (req, res, next) {
    res.send(deployed[req.params.name]);

});

router.get('/deployed/:name', function (req, res, next) {
    res.send(compiledContracts[req.params.name]);

});


router.get('/contracts/add', function (req, res, next) {

    res.send('<h2>Add New contract</h2> <br><form method="post" action="/rpc/contract/contracts/add">Contract Name :<br> <input type="text" id="name" name="name"><br> Contract Code : ' +
        '<br><textarea id ="contract" name="contract" style="margin: 0px; width: 800px; height: 850px;"></textarea><br><br><br> <button type="submit">submit contract</button> </form>');

});

router.post('/contracts/add', function (req, res, next) {
    console.log('add new contract');

    if (req.param("contract") && req.param("name")) {
        var text = req.param("contract");
        text = text.replace(/(\r\n|\n|\r)/gm, "");

        contracts[req.param("name")] = text;
        res.send('Contract : ' + req.param("name") + ' added.<br><a href="/rpc/contract/compile/' + req.param("name") + '">compile the contract</a>');

    } else {
        res.send('error');
    }
});


router.get('/content/:name', function (req, res, next) {
    res.send(contracts[req.params.name]);

});


router.post('/execute/:contract/:function', function (req, res, next) {
    executeTransaction(req, res);
});
router.get('/execute/:contract/:function', function (req, res, next) {
    executeTransaction(req, res);
});

function executeTransaction(req, res, cb) {

    var fun = cb || function (e, d) {
            if (!e) {
                res.send(d);
                console.log(d);
            }
        };
    var deployedContract = deployed[req.params.contract];
    if (deployedContract) {
        var paramNames = cache[req.params.contract][req.params.function];
        var params = [];
        for (var i = 0; i < paramNames.length; i++) {
            params.push(req.param(paramNames[i]));
        }
        if (!req.param("constant")) {
            params.push({from: web3.eth.accounts[0], gas: 3000000});
        }
        params.push(fun);

        deployedContract[req.params.function](...params);

    } else {
        res.send('error ');
    }

    console.log(req.params);
}

router.get('/:name/functions', function (req, res, next) {
    if (deployed[req.params.name]) {

        res.send(getFunctionsHtml(req.params.name));
    }
    else {
        res.send('Contract ' + req.params.name + ' not mined yet.');
    }


});
router.get('/console/:name', function (req, res, next) {
    if (deployed[req.params.name] && deployed[req.params.name].address) {
        res.send('var ' + req.params.name + ' = eth.contract(' + deployed[req.params.name].abi + ').at(\'' + deployed[req.params.name].address + '\');');
    }
    else {
        res.send('Contract ' + req.params.name + ' not mined yet or not exist.');
    }
});

function getFunctionsHtml(contractName,abi) {
    var abiData = abi ? abi : compiledContracts[contractName].abi;
    var f = '<head><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script><script></script></head>'
    f += '<h2> ' + contractName + ' functions: </h2><br>';
    cache[contractName] = {};
    for (var i = 0; i < abiData.length; i++) {
        var func = abiData[i];
        if (func.type == 'function') {
            var params = [];
            f += func.name + ' :<br> <form method="post" action="/rpc/contract/execute/' + contractName + '/' + func.name + '"><br>';
            if (func.inputs != undefined) {
                for (var j = 0; j < func.inputs.length; j++) {
                    var name = func.inputs[j].name;

                    name != "" ? name : "a" + j
                    params.push(name);
                    var param = name + " : ";
                    if (func.inputs[j].type == 'string') {
                        param += '<textarea  id="' + name + '" name="' + name + '"></textarea><br>'
                    } else {
                        param += '<input type="text" id="' + name + '" name="' + name + '"><br>';
                    }

                    if (func.constant) {
                        param += '<input type="hidden" id="constant" name="constant" value="true"><br>';
                    }

                    f += param;

                }
                cache[contractName][func.name] = params;
            }

            f += ' Execute: <button name="subject" type="submit">' + func.name + '</button> </form>';

        }
    }

    return f;


}

router.get('/compile/:name', function (req, res, next) {
    var cont = contracts[req.params.name];

    console.log(cont);
    var compiled = web3.eth.compile.solidity(cont);
    if (compiled) {
        console.log(compiled);
        var contract = {};
        contract.abi = compiled[req.params.name].info.abiDefinition;
        contract.code = compiled[req.params.name].code;
        compiledContracts[req.params.name] = contract;
        res.send(req.params.name + ' Compiled successfully! <br><br><a href="/rpc/contract/deploy/' + req.params.name + '">Deploy the contract</a>');
    } else {
        res.send("error! could not compile contract : " + req.params.name);
    }

});

router.post('/compile', function (req, res, next) {
    var data = req.body.data;
    var com = compile(data);
    compiledContracts[com.name] = com;
    console.log(com);
    res.send(com);

});
function compile(contractTxt) {
    var data = contractTxt.replace(/(\r\n|\n|\r)/gm, "");

    var contract = {};
    try {
        var compiled = web3.eth.compile.solidity(data);
    } catch (err) {
        contract.success = false;
        contract.message = err.message;
    }
    if (compiled) {
        if (!compiled.message) {
            contract.success = true;
            contract.name = Object.keys(compiled)[0];
            contract.code = compiled[contract.name].code;
            contract.abi = compiled[contract.name].info.abiDefinition;
            ;
        } else {
            contract.success = false;
            contract.message = compiled.message;
        }
    }
    return contract;
}

router.get('/data/all', function (req, res, next) {
    var all = [];
    Object.keys(deployed).forEach(function (key) {
            var cont = deployed[key];
            all.push({
                status: cont.address ? 'Ready' : cont.deployError ? 'Error' : 'Not mined yet',
                css: cont.address ? 'info' : cont.deployError ? 'danger' : 'warning',
                name: key,
            });
        }
    );

    res.send(all);

});


router.post('/deploy', function (req, res, next) {
    var data = req.body.data;
    var com = compile(data);
    if (com.success) {
        deployed[com.name] = {};
        web3.eth.contract(com.abi).new({
            from: web3.eth.accounts[0],
            data: com.code,
            gas: 2000000
        }, function (err, contInstance) {
            if (err) {
                console.error(err);
                cont.deployError = true;
            } else if (contInstance.address) {
                deployed[com.name] = contInstance;
                console.log('address : ' + contInstance.address);
                console.log('deployed : ', com.name);

            }
        });

        res.send({name: com.name, contractDeployed: true});
    } else {
        res.send({contractDeployed: false, deployError: true});
    }

});

router.get('/events/:name/init', function (req, res, next) {

    initWatchers(req.params.name);

    res.send("done");

});

function initWatchers(name) {
    var abi = deployed[name].abi;

    events[name] = {};
    for (var i = 0; i < abi.length; i++) {
        var func = abi[i];
        if (func.type == 'event') {
            events[name][func.name] = [];
            deployed[name][func.name]({}, {fromBlock: 0, toBlock: 'latest'}).watch(function (error, event) {
                if (!error) {
                    events[name][event.event].push(event.args);
                    console.log(event.args);
                }
            });


        }

    }
}

router.get('/events/data/:name/:event', function (req, res, next) {
    if (events[req.params.name] && events[req.params.name][req.params.event]) {
        res.send(events[req.params.name][req.params.event]);

    } else {
        res.send('error');
    }
});


// TODO: remove after ISDX DEMO

var init = false;
var isdxTxt='contract ISDX {mapping(uint => Company) public companies; mapping(uint => Trade) public trades; mapping(uint => string) public holders; mapping(uint => mapping(uint => uint)) public data; uint public compCount=1; uint public holdersCount=1; uint public tradeCount=1; address owner; struct  Company {string name; string symbol; string mifidStatus; string isin; string ccy; uint shares; } struct Trade{uint from; uint to; uint compId; uint amount; } function ISDX() {owner = tx.origin; } event companyAdded(uint compId,string name, string symbol, string mifid, string isin,uint shares,uint holder,string ccy); function registerCompany(string _name, string _symbol, string _mifid, string _isin,string _ccy,uint shares,uint holder){if (msg.sender == owner){companies[compCount++] = Company(_name, _symbol, _mifid,_isin,_ccy,shares); companyAdded(compCount-1,_name, _symbol, _mifid,_isin,shares,holder,_ccy); data[compCount-1][holder] = shares; } } event tradeAdded(uint tradeId,uint time, uint compId,uint amount, string price,uint from,uint to); event shareHolder(uint id,string name); function getShareBalance(uint shareId, uint holderId) constant returns (uint){return data[shareId][holderId]; } function addTrade(uint _from, uint _to,uint _compId,uint _amount, string _price){if (msg.sender == owner){if(data[_compId][_from] - _amount >= 0 ){uint time = now; data[_compId][_from]= data[_compId][_from] - _amount; data[_compId][_to] = data[_compId][_to] + _amount; trades[tradeCount++] = Trade(_from, _to,_compId, _amount); tradeAdded(tradeCount-1,time,_compId, _amount,  _price,_from,_to); } } } function addShareHolder(string _name){if (msg.sender == owner){holders[holdersCount++] = _name; shareHolder(holdersCount -1, _name); } } }';
var isdxAbi =[{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"trades","outputs":[{"name":"from","type":"uint256"},{"name":"to","type":"uint256"},{"name":"compId","type":"uint256"},{"name":"amount","type":"uint256"}],"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"companies","outputs":[{"name":"name","type":"string"},{"name":"symbol","type":"string"},{"name":"mifidStatus","type":"string"},{"name":"isin","type":"string"},{"name":"ccy","type":"string"},{"name":"shares","type":"uint256"}],"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"holders","outputs":[{"name":"","type":"string"}],"type":"function"},{"constant":false,"inputs":[{"name":"_name","type":"string"}],"name":"addShareHolder","outputs":[],"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"},{"name":"","type":"uint256"}],"name":"data","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"constant":true,"inputs":[],"name":"holdersCount","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"constant":false,"inputs":[{"name":"_name","type":"string"},{"name":"_symbol","type":"string"},{"name":"_mifid","type":"string"},{"name":"_isin","type":"string"},{"name":"_ccy","type":"string"},{"name":"shares","type":"uint256"},{"name":"holder","type":"uint256"}],"name":"registerCompany","outputs":[],"type":"function"},{"constant":true,"inputs":[{"name":"shareId","type":"uint256"},{"name":"holderId","type":"uint256"}],"name":"getShareBalance","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"constant":true,"inputs":[],"name":"tradeCount","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"uint256"},{"name":"_to","type":"uint256"},{"name":"_compId","type":"uint256"},{"name":"_amount","type":"uint256"},{"name":"_price","type":"string"}],"name":"addTrade","outputs":[],"type":"function"},{"constant":true,"inputs":[],"name":"compCount","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"inputs":[],"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"compId","type":"uint256"},{"indexed":false,"name":"name","type":"string"},{"indexed":false,"name":"symbol","type":"string"},{"indexed":false,"name":"mifid","type":"string"},{"indexed":false,"name":"isin","type":"string"},{"indexed":false,"name":"shares","type":"uint256"},{"indexed":false,"name":"holder","type":"uint256"},{"indexed":false,"name":"ccy","type":"string"}],"name":"companyAdded","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"tradeId","type":"uint256"},{"indexed":false,"name":"time","type":"uint256"},{"indexed":false,"name":"compId","type":"uint256"},{"indexed":false,"name":"amount","type":"uint256"},{"indexed":false,"name":"price","type":"string"},{"indexed":false,"name":"from","type":"uint256"},{"indexed":false,"name":"to","type":"uint256"}],"name":"tradeAdded","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"name","type":"string"}],"name":"shareHolder","type":"event"}];
var holders = {};
var companies = {};
var compData = {};
var txHashes = {};
var balances = {};
var trades = [];
var isdxPath = './data/ISDX.contract';


router.get('/events/ISDX/runDemo', function (req, res, next) {
    var com = compile(isdxTxt);
    web3.eth.contract(isdxAbi).new({
        from: web3.eth.accounts[0],
        data: com.code,
        gas: 3000000
    }, function (err, contInstance) {
        if (err) {
            console.error(err);
            cont.deployError = true;

            res.send({status : 'error'});
        } else if (contInstance.address) {
            deployed[com.name] = contInstance;
            console.log('deployed : ', com.name);
            console.log('address : ' + contInstance.address);
            fs.writeFileSync(isdxPath, JSON.stringify({address: contInstance.address, abi: isdxAbi,name:'ISDX'}));
            getFunctionsHtml('ISDX',isdxAbi);
            initIsdxListeners();

            res.send({status : 'done'});
        }
    });

});


router.get('/events/ISDX/loadDemo', function (req, res, next) {
    fs.access(isdxPath, fs.F_OK, function (err) {
        if (!err) {
            var contents = fs.readFileSync(isdxPath).toString();
            var cont = JSON.parse(contents);
            var isdxDeployed = web3.eth.contract(cont.abi).at(cont.address);
            deployed['ISDX'] = isdxDeployed;
            getFunctionsHtml('ISDX',cont.abi);
            initIsdxListeners();
            res.send({status : 'done'});

        } else {
            res.send({error : 'Demo Not Found. try /rpc/contract/events/ISDX/runDemo'})
        }
    });


});


function initIsdxListeners(){
    var name = 'ISDX';

    holders = {};
    companies = {};
    txHashes = {};
    compData = {};
    balances = {};
    trades = [];
    if(deployed[name]) {
        deployed[name]['shareHolder']({}, {fromBlock: 0, toBlock: 'latest'}).watch(function (error, event) {
            if (!error) {
                holders[event.args.id] = event.args.name;
                console.log('share holder ', event.args);
            }
        });
        
        setTimeout(function () {
            deployed[name]['companyAdded']({}, {fromBlock: 0, toBlock: 'latest'}).watch(function (error, event) {
                if (!error) {

                    var company = {
                        name: event.args.name,
                        symbol: event.args.symbol,
                        mifid: event.args.mifid,
                        isin: event.args.isin,
                        totalShares: parseInt(event.args.shares),
                        initialHolder: holders[event.args.holder],
                        ccy: event.args.ccy
                    };
                    companies[event.args.compId] = company;
                    balances[event.args.compId] = {};
                    balances[event.args.compId][company.initialHolder] = parseInt(company.totalShares);
                    console.log(event.args);
                }
            });
        },2000);

        setTimeout(function () {
            deployed[name]['tradeAdded']({}, {fromBlock: 0, toBlock: 'latest'}).watch(function (error, event) {
                if (!error) {
                    if(!txHashes[event.transactionHash]) {
                        txHashes[event.transactionHash] = true;
                        var tx = {};
                        var trade = event.args;
                        tx.blockNumber = event.blockNumber;
                        tx.address = event.address;
                        tx.blockHash = event.blockHash;
                        tx.txHash = event.transactionHash;
                        trade.tx = tx;
                        trade.amount = parseInt(event.args.amount);
                        trade.fromName = holders[parseInt(event.args.from)];
                        trade.toName = holders[parseInt(event.args.to)];
                        trade.ccy = companies[event.args.compId].ccy;
                        trade.compName = companies[event.args.compId].name;
                        var tTime = new Date(parseInt(event.args.time) * 1000).toLocaleString();
                        trade.time = tTime;

                        trades.push(trade);
                        updateIsdxBalance(parseInt(event.args.compId), parseInt(event.args.to));
                        updateIsdxBalance(parseInt(event.args.compId), parseInt(event.args.from));
                        updateCompData(event, tTime, tx);

                        console.log('trade added params ',trade,event.args);
                    }
                }
            });
        },5000);
    }
}

router.get('/events/ISDX/start', function (req, res, next) {
    initIsdxListeners();

    res.send('done');


});
function updateCompData(eventt,tTime,tx) {
    var data = eventt.args;

    if (!compData[data.compId]) {
        var d = {};
        d.low = parseFloat(data.price);
        d.high = parseFloat(data.price);
        d.volume = parseInt(data.amount);
        d.txs = 0;
        d.trades = [];
        compData[data.compId] = d;
    } else {
        if (compData[data.compId].low > parseFloat(data.price)) {
            compData[data.compId].low = parseFloat(data.price);
        }
        if (compData[data.compId].high < parseFloat(data.price)) {
            compData[data.compId].high = parseFloat(data.price);
        }
        compData[data.compId].volume += parseInt(data.amount);
    }
    compData[data.compId].lastTx = tTime;
    compData[data.compId].trades.push({
        id: parseInt(data.tradeId),
        from: parseInt(data.from),
        to: parseInt(data.to),
        fromName: holders[parseInt(data.from)],
        toName: holders[parseInt(data.to)],
        price: parseFloat(data.price),
        amount: parseInt(data.amount),
        time: tTime,
        timestamp: parseInt(data.time),
        ccy : companies[data.compId].ccy,
        compName : companies[data.compId].name,
        tx :tx
    });
    compData[data.compId].lastPrice = parseFloat(data.price);
    compData[data.compId].txs++;

}

router.get('/events/ISDX/shareHolder', function (req, res, next) {
    res.send(JSON.stringify(holders));

});

router.get('/events/ISDX/trades', function (req, res, next) {
    res.send(trades);
});

router.get('/events/ISDX/trades/:compId', function (req, res, next) {
    var ret = getCompanyDataById(req.params.compId, true);

    res.send(JSON.stringify(ret.trades ? ret.trades : ret));
});

router.get('/isdx/isdxTransactions', function (req, res, next) {
    res.send({tx: trades.length});
});

router.get('/events/ISDX/balance', function (req, res, next) {
    res.send(JSON.stringify(balances));
});

router.get('/events/ISDX/balance/:compId', function (req, res, next) {
    if (balances[req.params.compId]) {
        res.send(JSON.stringify(balances[req.params.compId]));
    } else {
        res.send({error: 'not found'});
    }
});


router.get('/events/ISDX/companies', function (req, res, next) {
    res.send(JSON.stringify(companies));

});
router.get('/ISDX/companies/data', function (req, res, next) {
    var ret = [];
    Object.keys(companies).forEach(function (key) {
            ret.push(getCompanyDataById(key));

        }
    )
    res.send(ret);

});


router.get('/ISDX/company/:id', function (req, res, next) {
    res.send(getCompanyDataById(req.params.id, true));
});


function getCompanyDataById(key, additional) {
    if (companies[key]) {
        var ret = {
            id: key,
            name: companies[key].name,
            sec: 'ORD',
            totalShares: companies[key].totalShares,
            ccy: companies[key].ccy,
            vol: compData[key] ? compData[key].volume : 0,
            last: compData[key] ? compData[key].lastTx : '-',
            low: compData[key] ? compData[key].low : 0,
            high: compData[key] ? compData[key].high : 0
        };

        if (additional) {
            ret.lastPrice = compData[key] ? compData[key].lastPrice : 0;
            ret.isin = companies[key].isin;
            ret.mifid = companies[key].mifid;
            ret.mifid = companies[key].mifid;
            ret.symbol = companies[key].symbol;
            ret.trades = compData[key] ? compData[key].trades : [];
        }

        return ret;

    } else {
        return {error: 'not found'};
    }
}

function updateIsdxBalance(shareId, holderId) {
    deployed['ISDX'].getShareBalance(shareId, holderId, function (e, d) {
        if (!e) {
            balances[shareId][holders[parseInt(holderId)]] = parseInt(d);
            console.log('updating share balance of ' + holders[parseInt(holderId)] + ' to: ' + parseInt(d));
        }
    });

}



router.post('/ISDX/addTrade', function (req, res, next) {
    if (validateTrade(req)) {
        req.params['contract'] = 'ISDX';
        req.params['function'] = 'addTrade';

        executeTransaction(req, res);
    } else {
        res.send({error:'error'});
    }
});


router.get('/ISDX/getHolderInfo/:id', function (req, res, next) {
    if(holders[req.params.id]){
        var ret={};
        ret.name = holders[req.params.id];
        ret.balances = [];
        Object.keys(balances).forEach(function (key) {
            if(balances[key][ret.name]){
                var last =compData[key] && compData[key].lastPrice?compData[key].lastPrice:0;
                ret.balances.push({id:key,name:companies[key].name,lastPrice:last,amount:balances[key][ret.name],ccy:companies[key],est:Math.round(last*balances[key][ret.name])});
            }

        });
        res.send(ret);

    } else {res.send({error:'error'});}
});


function validateTrade(req) {
    return balances[req.body._compId][holders[req.body._from]] && balances[req.body._compId][holders[req.body._from]] >= req.body._amount;
}


module.exports = router;

