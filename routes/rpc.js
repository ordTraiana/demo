var express = require('express');
var router = express.Router();
var Web3 = require('web3');


var rpcNode = 'http://ec2-54-173-23-102.compute-1.amazonaws.com:8541';

var web3 = new Web3(new Web3.providers.HttpProvider(rpcNode));


/* GET rpc listing. */
router.get('/', function (req, res, next) {

    res.send(rpcNode);

});


router.get('/data', function (req, res, next) {
    var data = {};
    data.host = rpcNode;
    if (web3.isConnected()) {
        data.connected = true;
        web3.net.getPeerCount(function (error, result) {
            if (!error) {
                data.peers = result;
                data.nodes = data.peers + 1;
                data.blockNumber = web3.eth.blockNumber;
                res.send(data);
            }
        });
    } else {
        data.connected = false;
        res.send(data);
    }
});


router.get('/peerCount', function (req, res, next) {
    console.log(web3.currentProvider);
    console.log('isConnected : ', web3.isConnected());

    web3.net.getPeerCount(function (error, result) {
        if (!error) {
            console.log("peers: ", result);
            res.send('peers: ' + result);
        }
    });
});

router.get('/test', function (req, res, next) {
    var a = {DB: 'tahat', DB1: 3};
    console.log(a);

    var s = "Function : <br><form id='aaa' method='POST' action='/rpc/submit'> <input type='text' name='param111' id='param1'> <input type='submit'> </form>";
    res.send(s);

});

;
router.get('/blockNumber', function (req, res, next) {
    res.send({block:web3.eth.blockNumber});
});
router.get('/pendingTx1', function (req, res, next) {

    res.send(web3.eth.getBlockTransactionCount("pending"));

});

router.get('/gen/:count', function (req, res, next) {
    res.send(generate(req.params.count));
});


router.get('/gen1/:count', function (req, res, next) {
    res.send(generateContract(req.params.count));
});
function generate(count) {

    var text = 'contract Test{\n';
    var ctor = 'function Test(){counter=0;}\n\n';
    var struct ='struct Deal {\n';

    var addSign = 'function addDeal(';
    var addContent='';
    var members = 'uint public counter;\n' +
        'mapping(uint => mapping(string => string)) data; \n';
    for (var i = 0; i < count; i++) {
        addSign+=i>0 ?', string _parameter'+i +' ' : ' string _parameter'+i;
        if(i%10 == 0){addSign+='\n';}

        addContent+='data[counter]["parameter'+i+'"] = _parameter'+i +';\n';

        struct +='string parameter'+i+';\n';


    }
    struct +='}\n';
    addSign+='){\n';
    addContent +='counter++;\n}\n';

    text +=members + ctor + addSign+addContent + '}';
    console.log(text);

    return text;
}


function generateContract(count) {

    var text = 'contract Test{\n';
    var ctor = 'function Test(){counter=0;}\n\n';


    var addSign = 'function addDeal(';
    var addContent='';
    var members = 'uint public counter;\n';
    for (var i = 0; i < count; i++) {
        addSign+=i>0 ?', string _parameter'+i +' ' : ' string _parameter'+i;
        if(i%10 == 0){addSign+='\n';}

        addContent+='parameter'+i+' = _parameter'+i +';\n';

        members +='string parameter'+i+';\n';


    }

    addSign+='){\n';
    addContent +='counter++;\n}\n';

    text +=members + ctor + addSign+addContent + '}';
    console.log(text);

    return text;
}
module.exports = router;